<?php
    
    //Retrieve the session key
    function GetSessionKey()
    {
        if(!isset($_SESSION['id']))
        {
            $_SESSION['id']=NULL;
        }
        return $_SESSION['id'];
    }
    
    //Set the session key
    function SetSessionKey($key)
    {
         $_SESSION['id']=$key;
    }
    
    //Retrieve the session type
    function GetSessionType()
    {
        if(!isset($_SESSION['cnt_type']))
        {
            $_SESSION['cnt_type']=NULL;
        }
        return $_SESSION['cnt_type'];
    }
    
    function GetSessionName()
    {
        if(!isset($_SESSION['cnt_name']))
        {
            $_SESSION['cnt_name']=NULL;
        }
        return $_SESSION['cnt_name'];
    }
    
    //Set the session type according to a number given.
    //If the number isn´t between 0 and 2, the session type would de 2
    function SetSessionType($type)
    {
        GetSessionName();
        if($type>=1 && $type<="2")
        {
            $_SESSION['cnt_type']=$type;
            if($type=='2')
            {
                $_SESSION['cnt_name']="GUEST";
            }
            if($type=='1')
            {
                $_SESSION['cnt_name']="USER";
            }
            if($type=='0')
            {
                $_SESSION['cnt_name']="ADMIN";
            }
        }else{
            $_SESSION['cnt_type']="2";
            $_SESSION['cnt_name']="GUEST";
        }
    }
    
    //Destroys the actual session and its variables
    function SessionDestroyer()
    {
        session_unset();
        session_destroy();
    }
    
    /*
     *Blocker is a polimorfic function that initialize the type and key for a session
     *it then check if your allowed to be in the current page, if not redirects to an url
     *it uses the params:
     *$Session = Boolean function (0,1), that defines if you need an active session
     *$Url = url to redirect.
     */
    function Blocker($Session, $Url)
    {
        if(GetSessionKey()==NULL)
        {
            if(GetSessionType()==NULL)
            {
                SetSessionType("2");
            }
            //msgBox("1 You´re logged as: ".GetSessionType());
        }
        if($Session)
        {
            if(GetSessionKey()!=NULL)
            {
                //msgBox("2 You´re logged as: ".GetSessionType());
                redirect($Url,0);
            }
        }else{
            if(GetSessionKey()==NULL)
            {
                //msgBox("2 You´re logged as: ".GetSessionType());
                redirect($Url,0);
            }
        }
    }
?>