<?php
    session_start();

    //Include the files needed to work
    require ("../adapter/mysqlconnect.php");
    include("../controllers/sessionmanager.php");
    include ("util.php");
    
    //Set the paths to redirect
    $index="../view/login.php";
    $main="../view/dashboard.php";
    
    //Get the _POST data from the form
    $usr = $_POST['usr'];
    $pass = $_POST['pass'];
    
    //Use the method login from logger php
    $exect=login("users", "user", "pass", $usr, $pass, "user");
    
    //If succed redirect to the dashboard and creates a session, else refresh the page
    if($exect!=NULL)
    {
        //msgBox("Bienvenido: ".$exect);
        SetSessionType("1");
        SetSessionKey($exect);
        redirect($main,0);
    }
    else
    {
        msgBox('Usuario o contraseña invalidos, por favor verifique sus datos');
        redirect($index,10);
    }
?>

