<?php
  session_start();
  
  include("../controllers/sessionmanager.php");
  include("../controllers/util.php");
  
  /*
   *Validates if theres an active session, if not; set the user type as GUEST
   *If theres an active session then redirects this pages to the dashboard
   *to avoid double loggin and session overwriting.
   */
  $main="../view/main.php";
  Blocker(1, $main);  
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Login</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="../styles/materialize/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../styles/materialize/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">PROMO</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#"></a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#"></a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>

  <div class="container">
      <div class="row center">
          <div class="col s12 m4  offset-m4 offset-l4">
              <h1 class="header center orange-text">Acceder</h1>
              <form role="form" id="login" action="../controllers/logger.php" method="post">
                  <fieldset>
                      <div class="input-field col s12">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="usr" placeholder="Usuario" name="usr" type="text" onchange="cleaner('response');" value="" autofocus >
                      </div>
                      
                      <div class="input-field col s12">
                          <i class="material-icons prefix">lock</i>
                          <input id="pass" placeholder="Contraseña" name="pass" type="password" onchange="cleaner('response');" value="" >
                      </div>
                      
                      <a class="btn orange waves-effect waves-yellow" onclick="validate();" >Entrar</a>
                      
                      <button id="submit" type="submit" class="btn orange waves-effect waves-yellow" style="display:none" ></button>
                      
                      <br/>
                      <br/>
                      
                      <div id="response" class="card-3d white-text orange s12 m4 l4 offset-m4 offset.l4">
                        
                      </div>
                      
                  </fieldset>
              </form>
              
          </div
      </div>
  </div>
  </div>
        
  <br/>
  <br/>
  <br/>
  <br/>
  <br/> 
  
  <footer class="page-footer light-blue">
    <div class="container">
      <div class="row">
        <div class="col l12 s12">
          
        </div>
      </div>
    </div>
    <div class="footer-copyright blue">
      <div class="container center">
      
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="../styles/materialize/js/materialize.js"></script>
  <script src="../styles/materialize/js/init.js"></script>
  <script type="text/javascript" src="../scripts/login/validator.js"></script>
 
  </body>
</html>
