<?php
  session_start();
  
  include("../controllers/util.php");
  include("../controllers/sessionmanager.php");
  
  /*
   *Validates if theres an active session, if not; redirect the page to the login page
   *to avoid unauthorized access via session, further acces grant would be
   *determined by an .htacces file
   */
  //
  $index="../view/login.php";
  Blocker(0,$index);
  //msgBox("User type: ".GetSessionType()."-".GetSessionName().". Username: ".GetSessionKey());
?>
<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="../public/icons/agent.png" />
	<link rel="stylesheet" type="text/css" href="../styles/materialize/css/materialize.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<script type="text/javascript" src="../scripts/jquery/jquery.js""></script>
	<script type="text/javascript" src="../styles/materialize/js/materialize.min.js""></script>
	<script type="text/javascript" src="../scripts/jquery/jquery-wrapper.js""></script>

	<!--Script modal-->
	<script type="text/javascript" src="../scripts/modal.js""></script>
</head>
<body>
	<header>
		<nav>
		    <div class="nav-wrapper  blue darken-3">
				<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<a href="#" class="brand-logo center">Dashboard</a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li>
						<!-- Dropdown Trigger -->
						<a class='dropdown-trigger btn blue'  data-target='dropdown2'>Usuario</a>
					</li>
				</ul>
		    </div>
		  </nav>
	</header>


  <!--Menu de dispositivos moviles-->
  <ul id="slide-out" class="sidenav">
    <li>
				<img src="../public/img/office.jpg" width="100%">
	</li>
    <li><a class="dropdown-trigger" data-target='dropdown1'><i class="material-icons left">arrow_drop_down</i>Usuario</a></li>
  </ul>



<ul id='dropdown1' class='dropdown-content'>
    <li><a href="login.php" onclick="<?php SessionDestroyer(); ?>"><i class="material-icons left">input</i>Salir</a></li>
</ul>

<ul id='dropdown2' class='dropdown-content'>
    <li><a href="login.php" onclick="<?php SessionDestroyer(); ?>"><i class="material-icons left">input</i>Salir</a></li>
  </ul>

	<div class="collection ">
	    <a href="#!" onclick="edit('value');" class="collection-item black-text"><img src="../public/icons/promotor.png" width="20px" style="margin-right: 2%;"><span class="badge"><img src="../public/img/yellow.png" width="12px"></span>Nombre completo de un promotor</a>
	    <a href="#!" onclick="edit('value');"  class="collection-item black-text"><span class="badge"><img src="../public/img/green.png" width="12px"></span><img src="../public/icons/promotor.png" width="20px" style="margin-right: 2%;">Nombre completo de un promotor</a>
	    <a href="#!" onclick="edit('value');" class="collection-item black-text"><img src="../public/icons/promotor.png" width="20px" style="margin-right: 2%;"><span class="badge"><img src="../public/img/red.png" width="12px"></span>Nombre completo de un promotor</a>
	</div>
<div id="modalDiv"></div>


</body>
</html>