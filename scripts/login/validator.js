/*
 *Function used only to validate if the users input the rigth
 *information.
 */
function cleaner(listener)
{
    var response = document.getElementById(listener);
    if(this.value!='' && response.innerHTML!="")
    {
        response.innerHTML="";
    }
}

//A genereic toUpper function
function upper(e)
{
    e.value = e.value.toUpperCase();
}

//A genereic toLowwer function
function lower(e)
{
    e.value = e.value.toLowerCase();
}

function validate()
{
    var user = document.getElementById("usr");
    var pass = document.getElementById("pass");
    var response = document.getElementById("response");
    
    if (user.value == '' && pass.value=='')
    {
        response.innerHTML="";
        response.innerHTML="Tienes que introducir un usuario y contraseña";
    }
    if (user.value == '' && pass.value!='' )
    {
        response.innerHTML="";
        response.innerHTML="Tienes que introducir un usuario";
    }
    if (pass.value == '' && user.value!='')
    {
        response.innerHTML="";
        response.innerHTML="Tienes que introducir una contraseña";
    }
    if (pass.value != '' && user.value!='')
    {
        document.getElementById("submit").click();
    }
}